/****** Object:  Table [dbo].[tbl_user]    Script Date: 02/09/2022 16.14.05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_user]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_user]
GO

/****** Object:  Table [dbo].[tbl_user]    Script Date: 02/09/2022 16.14.05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_user](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[namalengkap ] [varchar](255) NULL,
	[username ] [varchar](255) NULL,
	[password ] [varchar](255) NULL,
	[status ] [char](255) NULL,
 CONSTRAINT [PK__tbl_user__CBA1B257CB90721F] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


